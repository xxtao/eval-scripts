#!/bin/bash
set -e -x

CF_HOME=$HOME

cf login -a https://api.cw-vdr-labs1.elpaaso.net -u admin -p OrangePaas --skip-ssl-validation
cf update-quota default -m 50G
cf create-user itao TMPpwd
cf create-org lizard
cf target -o lizard
cf create-space prod
cf set-space-role itao lizard prod SpaceManager
cf set-space-role itao lizard prod SpaceDeveloper
cf space-users lizard prod
cf login -u itao -p TMPpwd

cf login -a https://api.cw-vdr-labs2.elpaaso.net -u admin -p OrangePaas --skip-ssl-validation
cf update-quota default -m 50G
cf create-user itao TMPpwd
cf create-org lizard
cf target -o lizard
cf create-space prod
cf set-space-role itao lizard prod SpaceManager
cf set-space-role itao lizard prod SpaceDeveloper
cf space-users lizard prod
cf login -u itao -p TMPpwd

cf login -a https://api.cw-vdr-labs3.elpaaso.net -u admin -p OrangePaas --skip-ssl-validation
cf update-quota default -m 50G
cf create-user itao TMPpwd
cf create-org lizard
cf target -o lizard
cf create-space prod
cf set-space-role itao lizard prod SpaceManager
cf set-space-role itao lizard prod SpaceDeveloper
cf space-users lizard prod
cf login -u itao -p TMPpwd
