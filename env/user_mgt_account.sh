#!/bin/bash
set -e -x

CF_HOME=$HOME

cf login -a https://api.cw-vdr-labs1.elpaaso.net -u admin -p OrangePaas -o system_domain --skip-ssl-validation
cf update-quota default -m 200G
cf create-user accountMgr TMPpwd
cf create-org account
cf target -o account
cf create-space prod
cf set-space-role accountMgr account prod SpaceManager
cf set-space-role accountMgr account prod SpaceDeveloper
cf space-users account prod
cf login -u accountMgr -p TMPpwd

cf login -a https://api.cw-vdr-labs2.elpaaso.net -u admin -p OrangePaas -o system_domain --skip-ssl-validation
cf update-quota default -m 200G
cf create-user accountMgr TMPpwd
cf create-org account
cf target -o account
cf create-space prod
cf set-space-role accountMgr account prod SpaceManager
cf set-space-role accountMgr account prod SpaceDeveloper
cf space-users account prod
cf login -u accountMgr -p TMPpwd

cf login -a https://api.cw-vdr-labs3.elpaaso.net -u admin -p OrangePaas -o system_domain --skip-ssl-validation
cf update-quota default -m 200G
cf create-user accountMgr TMPpwd
cf create-org account
cf target -o account
cf create-space prod
cf set-space-role accountMgr account prod SpaceManager
cf set-space-role accountMgr account prod SpaceDeveloper
cf space-users account prod
cf login -u accountMgr -p TMPpwd
