#!/bin/sh
set -ex

function clean(){
	cf ds lizard-eureka-service
	cf ds lizard-zipkin-service
	cf delete-orphaned-routes
}

CF_HOME=$HOME/thesis/orange_login/experiment/cf_home/site1
clean
CF_HOME=$HOME/thesis/orange_login/experiment/cf_home/site2
clean
CF_HOME=$HOME/thesis/orange_login/experiment/cf_home/site3
clean