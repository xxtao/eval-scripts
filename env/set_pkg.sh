#!/bin/sh
set -ex

CF_HOME=$HOME/thesis/orange_login/experiment/cf_home/site1
cf set-env lizard-catalog-service RUM_PATH lizard-catalog-service-2.0.0.jar
cf set-env lizard-eligibility-service RUM_PATH lizard-eligibility-service-2.0.0.jar