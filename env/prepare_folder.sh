#!/bin/bash
set -x

[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

cd $BASE_DIR
mkdir data
mkdir log
mkdir eval/tmp
mkdir lizard/tmp
mkdir tmp
mkdir -p env/login/cf_home/site1
mkdir -p env/login/cf_home/site2
mkdir -p env/login/cf_home/site3
