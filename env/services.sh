#!/bin/bash
set -ex

[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home

function create-user-provided-service {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	cf create-user-provided-service lizard-eureka-service -p "{\"uri\":\"http://lizard-discovery.cw-vdr-labs1.elpaaso.net\"}"
	cf create-user-provided-service lizard-zipkin-service -p "{\"uri\":\"http://lizard-zipkin.cw-vdr-labs1.elpaaso.net\"}"
}

create-user-provided-service site1
create-user-provided-service site2
create-user-provided-service site3