#!/bin/bash
set -x

[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

SCRIPT_DIR=$BASE_DIR/scripts
update="$SCRIPT_DIR/update.sh $@"

$update
while [ $? -ne 0 ]; do
	sleep 60
	$update
done
