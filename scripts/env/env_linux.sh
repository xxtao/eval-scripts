#!/bin/sh

# proxy for cf cli
export http_proxy=http://proxy:8080
export https_proxy=http://proxy:8080

# env for jmeter
PATH=$PATH:$HOME/Application/apache-jmeter-3.2/bin
export proxy_host=proxy
export proxy_port=8080
alias jmeter="jmeter --proxyHost $proxy_host --proxyPort $proxy_port -Djdk.tls.allowUnsafeServerCertChange=true -Dsun.security.ssl.allowUnsafeRenegotiation=true"