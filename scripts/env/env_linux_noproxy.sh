#!/bin/sh

# env for jmeter
PATH=$PATH:$HOME/Application/apache-jmeter-3.2/bin
alias jmeter="jmeter -Djdk.tls.allowUnsafeServerCertChange=true -Dsun.security.ssl.allowUnsafeRenegotiation=true"