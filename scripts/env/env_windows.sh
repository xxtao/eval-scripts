#!/bin/sh

# proxy for cf cli
export http_proxy=http://localhost:8888
export https_proxy=http://localhost:8888

# env for jmeter
export proxy_host=localhost
export proxy_port=8888
alias jmeter="jmeter.bat --proxyHost $proxy_host --proxyPort $proxy_port -Djdk.tls.allowUnsafeServerCertChange=true -Dsun.security.ssl.allowUnsafeRenegotiation=true"