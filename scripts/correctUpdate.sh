#!/bin/bash
set -x

[ $# -ne 6 ] && { echo "Usage: $0 CORRECTION_STATE CORRECTION_STRATEGY FINAL_STATE STRATEGY_INITIAL DURATION_LOG LOG_SUFFIX"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

SCRIPT_DIR=$BASE_DIR/scripts
OP_CONFIG=$BASE_DIR/lizard/operationConfigLessTO.json
CURRENT_ARCHIT=$BASE_DIR/tmp/current_state.txt

update="$SCRIPT_DIR/update.sh $3 $OP_CONFIG $4 $5 $6"
rollcorrection="$SCRIPT_DIR/update.sh $1 $OP_CONFIG $2 $5 $6"

$update
while [ $? -ne 0 ]; do
	ERROR_MSG=$(cat $CURRENT_ARCHIT)
	if [[ $ERROR_MSG == *"Timeout during waiting wait until microservice "*" running"* ]]; then
		$rollcorrection
		update=$rollcorrection
	else
		sleep 5
		$update
	fi
done
