#!/bin/bash
set -x
[ $# -ne 6 ] && { echo "Usage: $0 FINAL_STATE NEXT_STATE STRATEGY_INITIAL STRATEGY_CONFIG OP_CONFIG LOG_SUFFIX"; exit 1; }

SCRIPT_DIR="$(dirname "$0")" # framework.sh is supposed to in the same dir with this script

FINAL_STATE="$1"
NEXT_STATE="$2"
STRATEGY_INITIAL="$3"
STRATEGY_CONFIG="$4"
OP_CONFIG="$5"
LOG_SUFFIX="$6"

case "$STRATEGY_INITIAL" in
	S)		STRATEGY="StraightStrategy" ;;
	BG)		STRATEGY="BlueGreenStrategy" ;;
	C)		STRATEGY="CanaryStrategy" ;;
	M)		STRATEGY="BlueGreenCanaryMixStrategy" ;;
	*)		exit 1
esac

: > $NEXT_STATE
result=$($SCRIPT_DIR/framework.sh $LOG_SUFFIX next -a $FINAL_STATE -sn $STRATEGY -sc $STRATEGY_CONFIG -oc $OP_CONFIG)
exit_code=$?
echo $result | jq . > $NEXT_STATE
cat $NEXT_STATE
if [[ $exit_code != 0 ]]; then
  echo "Failed to get the next state!"
  exit $exit_code
fi
