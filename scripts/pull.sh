#!/bin/bash
set -x
[ $# -ne 3 ] && { echo "Usage: $0 SITES_FILE OPCONFIG_FILE LOG_SUFFIX"; exit 1; }

SCRIPT_DIR="$(dirname "$0")" # framework.sh is supposed to in the same dir with this script

SITES_FILE="$1"
OPCONFIG_FILE="$2"
LOG_SUFFIX="$3"

response=$($SCRIPT_DIR/framework.sh $LOG_SUFFIX pull -s $SITES_FILE -oc $OPCONFIG_FILE)
exit_code=$?
echo $response | jq .
if [[ $exit_code != 0 ]]; then
  echo "Failed to pull the current state!"
  exit $exit_code
fi
