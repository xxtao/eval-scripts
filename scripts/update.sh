#!/bin/bash
set -ex

[ $# -ne 6 ] && { echo "Usage: $0 FINAL_STATE OP_CONFIG STRATEGY_INITIAL DURATION_LOG LOG_SUFFIX USECASE"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

STATE_DIR=$BASE_DIR/$6
SCRIPT_DIR=$BASE_DIR/scripts
PID_DIR=$BASE_DIR/tmp
UPD_MSG=$BASE_DIR/tmp/current_state.txt

FINAL_STATE="$1"
OP_CONFIG="$2"
STRATEGY_INITIAL="$3"
DURATION_LOG="$4"
LOG_SUFFIX="$5"

case "$STRATEGY_INITIAL" in
	S)		STRATEGY="StraightStrategy" ;;
	BG)		STRATEGY="BlueGreenStrategy" ;;
	C)		STRATEGY="CanaryStrategy" ;;
	M)		STRATEGY="BlueGreenCanaryMixStrategy" ;;
	*)		exit 1
esac
STRATEGY_CONFIG="$STATE_DIR/$STRATEGY_INITIAL""config.json"
MID_FILE="$STATE_DIR/tmp/mid.json"

echo "$(date +"%s%3N") Start Updating to $FINAL_STATE"

function finish {
	set +e
	$SCRIPT_DIR/verify.sh $FINAL_STATE $OP_CONFIG $LOG_SUFFIX
	success=$?
	echo "$(date +"%s%3N") Finish Updating to $FINAL_STATE with $success"
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
	exit $success
}
trap finish EXIT

echo -n $(date +"%s%3N"), >> $DURATION_LOG

$SCRIPT_DIR/framework.sh $LOG_SUFFIX update -a $FINAL_STATE -sn $STRATEGY -sc $STRATEGY_CONFIG -oc $OP_CONFIG > $UPD_MSG 2>&1 &
echo $! >> $PID_DIR/framework.pid
wait
cat $UPD_MSG
