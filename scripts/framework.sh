#!/bin/bash
# this script configure the required env var, then executing the framework commands
[ $# -lt 1 ] && { echo "Usage: $0 LOG_SUFFIX [PARAMS]"; exit 1; }

[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }
[[ -z "${PROTOTYPE_DIR}" ]] && { echo "Missing env var: PROTOTYPE_DIR"; exit 1; }

LOG_SUFFIX="$1"
LOG_FILE=$BASE_DIR/log/framework_log_$LOG_SUFFIX.txt
ENV_DIR=$BASE_DIR/scripts/env

if [[ -v "WITHOUT_PROXY" ]]; then
	env=$ENV_DIR/env_linux_noproxy.sh
else
	unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)		env=$ENV_DIR/env_linux.sh ;;
	    MINGW*)     env=$ENV_DIR/env_windows.sh ;;
	    *)          exit 1
	esac
fi
source $env

java -Dlogfile.name=$LOG_FILE -jar $PROTOTYPE_DIR/target/prototype-template-engine-0.0.1-SNAPSHOT-shaded.jar "${@:2}"
