#!/bin/bash
set -x
[ $# -ne 3 ] && { echo "Usage: $0 STATE_FILE OPCONFIG_FILE LOG_SUFFIX"; exit 1; }

SCRIPT_DIR="$(dirname "$0")" # framework.sh is supposed to in the same dir with this script

STATE_FILE="$1"
OPCONFIG_FILE="$2"
LOG_SUFFIX="$3"

expected=$($SCRIPT_DIR/framework.sh $LOG_SUFFIX arrived -a $STATE_FILE -oc $OPCONFIG_FILE)
if [[ "$expected" != "true" ]]; then
	echo "The current state is not expected."
	exit 1
fi
