#!/bin/bash
set -x

[ $# -ne 1 ] && { echo "Usage: $0 PID_FILE"; exit 1; }

PID_FILE=$1
sudo kill -9 $(cat $PID_FILE)
: > $PID_FILE 
