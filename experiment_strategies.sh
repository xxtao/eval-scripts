#!/bin/bash
set -ex

[ $# -ne 3 ] && { echo "Usage: $0 STRATEGY_INITIAL(S, BG, C, or M) EXP_TIMES USECASE(lizard or account)"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

USECASE="$3"

STATE_DIR=$BASE_DIR/$USECASE
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log
USECASE_DIR=$BASE_DIR/env/usecase

OP_CONFIG=$STATE_DIR/operationConfig.json

STRATEGY_INITIAL="$1"
EXP_TIMES="$2"

LOG_SUFFIX="prototype_"$STRATEGY_INITIAL"_"$USECASE"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function init {
	$BASE_DIR/measure.sh $LOG_SUFFIX $USECASE
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
}

function finish {
	set +e
	$BASE_DIR/stopMeasure.sh
}
trap finish EXIT

function experiment {
	local initArchi=$1
	local finalArchi=$2
	for i in {1..3}; do
		$BASE_DIR/prepare_experiment.sh $STATE_DIR/$initArchi.json $USECASE && break
	done;
	echo "" >> $DURATION_LOG
	echo -n $finalArchi, >> $DURATION_LOG
	$SCRIPT_DIR/update.sh $STATE_DIR/$finalArchi.json $OP_CONFIG $STRATEGY_INITIAL $DURATION_LOG $LOG_SUFFIX $USECASE && echo "succeeded"
	sleep 5
}

init
cd $USECASE_DIR
for i in $(eval echo {1..$EXP_TIMES}); do
	experiment final1 final2
	experiment final2 final1
done
