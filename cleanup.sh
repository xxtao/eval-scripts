#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 USECASE"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

USECASE="$1"
STATE_DIR=$BASE_DIR/$USECASE
SCRIPT_DIR=$BASE_DIR/scripts
PID_DIR=$BASE_DIR/tmp
LOG_SUFFIX="tmp"
OP_CONFIG=$STATE_DIR/operationConfig.json
DURATION_LOG=$BASE_DIR/tmp_duration_prepare.csv
LOG_SUFFIX=cleanup

$SCRIPT_DIR/update.sh $STATE_DIR/clean.json $OP_CONFIG S $DURATION_LOG $LOG_SUFFIX $USECASE
