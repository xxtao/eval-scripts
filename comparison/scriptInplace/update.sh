#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 $VERSION $DURATION_LOG"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
catalog_path_v1=$BASE_DIR/env/usecase/lizard-catalog-service_1.0.0.jar
catalog_path_v2=$BASE_DIR/env/usecase/lizard-catalog-service_2.0.0.jar

VERSION="$1"
DURATION_LOG="$2"

function finish {
	echo -n $(date +"%s%3N") >> $DURATION_LOG
}
trap finish EXIT

function update_catalog {
	manifest="catalog_"$site".yml"
	catalog_path=catalog_path_$VERSION
	cf push -f $manifest -p ${!catalog_path}
}

function update_eligibility {
	case $VERSION in
		v1)
			cf unset-env eligibility "ELIGIBILITY_COMMERCIAL_OPERATIONS[REC]_LIBELLE" 
			;;
		v2)
			cf set-env eligibility "ELIGIBILITY_COMMERCIAL_OPERATIONS[REC]_LIBELLE" "New_version"
			;;
	esac
	cf restage eligibility
}

function update_site {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	update_catalog
	update_eligibility
}

echo "" >> $DURATION_LOG
echo -n $VERSION, >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG
update_site site1
update_site site2
update_site site3
