#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 $VERSION $DURATION_LOG"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
USECASE_DIR=$BASE_DIR/env/usecase

RETRY_MAX=5
RETRY_BACKOFF=5

VERSION="$1"
DURATION_LOG="$2"

function finish {
	success=$?
	set +e
	echo "$(date +"%s%3N") Finish Updating to $FINAL_STATE with $success"
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
	exit $success
}
trap finish EXIT

function cf_exec {
	local cf_cmd="$1 2>&1"
	local cmd_result=$(eval "$cf_cmd")
	if [[ $cmd_result == *"Not logged in"* ]]; then
		$LOGIN_DIR/$site.sh
		cmd_result=$(eval "$cf_cmd")
	fi
	if [[ $cmd_result == *"No "*" targeted"* ]]; then
		$LOGIN_DIR/$site.sh
		cmd_result=$(eval "$cf_cmd")
	fi
	local tried=0
	while [[ $cmd_result == *"FAILED"* ]]; do
		sleep $RETRY_BACKOFF
		cmd_result=$(eval "$cf_cmd")
		tried=$(($tried + 1))
		if [[ $tried = $RETRY_MAX ]]; then
			echo "Failed in $tried times retry."
			exit 1
		fi
	done
}

function update_catalog {
	manifest=$VERSION/"catalog_"$site"_"$VERSION".yml"
	cf_exec "cf push -f $manifest -t 120"
}

function update_eligibility {
	manifest=$VERSION/"eligibility_"$site"_"$VERSION".yml"
	cf_exec "cf push -f $manifest -t 180"
}

function update_site {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	$LOGIN_DIR/$site.sh
	update_catalog
	update_eligibility
}

echo "" >> $DURATION_LOG
echo -n $VERSION, >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG
cd $USECASE_DIR
update_site site1 &
update_site site2 &
update_site site3 &
wait
