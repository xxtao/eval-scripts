#!/bin/bash
set -ex
function cf_exec {
	local cmd_result=$(exec_log "$cf_cmd")
	while [[ $cmd_result == *"Not logged in"* ]] || [[ $cmd_result == *"No "*" targeted"* ]]; do
		$LOGIN_DIR/$site.sh
		cmd_result=$(exec_log "$cf_cmd")
	done
}
function deploy_canary {
	cf_exec "cf push $app_name -f $manifest -i 1 --hostname $tmp_hostname -t 180"
}
function update_route {
	cf_exec "cf map-route $app_name $domain --hostname $hostname"
	cf_exec "cf unmap-route $app_name $domain --hostname $tmp_hostname"
}
function scale {
	for instances in $(eval echo {2..$total_instances}); do
		cf_exec "cf scale $new_app -i $instances"
		cf_exec "cf scale $old_app -i $((total_instances+1-instances))"
	done
}
function remove_old {
	cf_exec "cf delete $app -f"
}
function rename_new {
	cf_exec "cf rename $app_name $app"
}
function transition {
	$1 catalog 1 &
	$1 catalog 2 &
	$1 eligibility 1 &
	$1 eligibility 2 &
	wait
}
transition deploy_canary 
transition update_route 
transition scale
transition remove_old
transition rename_new