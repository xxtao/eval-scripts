#!/bin/bash
set -ex

[ $# -ne 3 ] && { echo "Usage: $0 VERSION DURATION_LOG OP_LOG"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
USECASE_DIR=$BASE_DIR/env/usecase
ERROR_MSG=$BASE_DIR/tmp/script_error.txt

INSTANCES_NUM=3
RETRY_MAX=5
RETRY_BACKOFF=5

VERSION="$1"
DURATION_LOG="$2"
OP_LOG="$3"

function finish {
	success=$?
	set +e
	echo "$(date +"%s%3N") Finish Updating to $FINAL_STATE with $success"
	echo -n $(date +"%s%3N"), >> $OP_LOG
	echo -n $success >> $OP_LOG
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
	exit $success
}
trap finish EXIT

function exec_log {
	local exec_cmd="$1"
	echo "" >> $OP_LOG
	echo -n $exec_cmd, >> $OP_LOG
	echo -n $(date +"%s%3N"), >> $OP_LOG
	local exec_result=$(eval "$exec_cmd 2>&1" || true)
	echo -n $(date +"%s%3N"), >> $OP_LOG
	if [[ $exec_result == *"FAILED"* ]]; then
		exec_success=1
	else
		exec_success=0
	fi
	echo -n $exec_success >> $OP_LOG
	echo $exec_result
}

function cf_exec {
	local cf_cmd="$1"
	local cmd_result=$(exec_log "$cf_cmd")
	while [[ $cmd_result == *"Not logged in"* ]] || [[ $cmd_result == *"No "*" targeted"* ]]; do
		$LOGIN_DIR/$site.sh
		cmd_result=$(exec_log "$cf_cmd")
	done
	if [[ $cmd_result == *"Start unsuccessful"* ]]; then
		echo "Start unsuccessful" > $ERROR_MSG
		exit 1
	fi
	local tried=0
	while [[ $cmd_result == *"FAILED"* ]]; do
		sleep $RETRY_BACKOFF
		cmd_result=$(exec_log "$cf_cmd")
		tried=$(($tried + 1))
		if [[ $tried = $RETRY_MAX ]]; then
			echo "Failed in $tried times retry."
			exit 1
		fi
	done
}

function update_site {
	local siteNum=$1
	local site="site"$siteNum
	# local domain="cw-vdr-labs"$siteNum".elpaaso.net"
	export CF_HOME=$CF_HOME_DIR/$site/
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
}

function deploy_canary {
	local siteNum=$2
	update_site $siteNum
	local app=$1
	local manifest=$VERSION/$app"_"$site"_"$VERSION".yml"
	local app_name=$app"-UPDATING"
	local tmp_hostname=$app"-tmp"
	cf_exec "cf push $app_name -f $manifest -i 1 --hostname $tmp_hostname -t 180"
}

function update_route {
	local siteNum=$2
	update_site $siteNum
	local app=$1
	# local domain=$2
	local domain="cw-vdr-labs"$siteNum".elpaaso.net"
	local app_name=$app"-UPDATING"
	local tmp_hostname=$app"-tmp"
	local hostname="lizard-"$app
	cf_exec "cf map-route $app_name $domain --hostname $hostname"
	cf_exec "cf unmap-route $app_name $domain --hostname $tmp_hostname"
}

# scale up new version and scale down old version
function scale {
	local siteNum=$2
	update_site $siteNum
	local old_app=$1
	local total_instances=$INSTANCES_NUM
	local new_app=$old_app"-UPDATING"
	for instances in $(eval echo {2..$total_instances}); do
		cf_exec "cf scale $new_app -i $instances"
		cf_exec "cf scale $old_app -i $((total_instances+1-instances))"
	done
}

function remove_old {
	local siteNum=$2
	update_site $siteNum
	local app=$1
	cf_exec "cf delete $app -f"
}

function rename_new {
	local siteNum=$2
	update_site $siteNum
	local app=$1
	local app_name=$app"-UPDATING"
	cf_exec "cf rename $app_name $app"
}

echo "" >> $DURATION_LOG
echo -n $VERSION, >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG
: > $ERROR_MSG
cd $USECASE_DIR
deploy_canary catalog 1 &
deploy_canary catalog 2 &
deploy_canary catalog 3 &
deploy_canary eligibility 1 &
deploy_canary eligibility 2 &
deploy_canary eligibility 3 &
wait
#update_route catalog $domain &
update_route catalog 1 & 
update_route catalog 2 &
update_route catalog 3 &
#update_route eligibility $domain &
update_route eligibility 1 &
update_route eligibility 2 &
update_route eligibility 3 &
wait
#scale catalog $INSTANCES_NUM &
scale catalog 1 &
scale catalog 2 &
scale catalog 3 &
#scale eligibility $INSTANCES_NUM &
scale eligibility 1 &
scale eligibility 2 &
scale eligibility 3 &
wait
remove_old catalog 1 &
remove_old catalog 2 &
remove_old catalog 3 &
remove_old eligibility 1 &
remove_old eligibility 2 &
remove_old eligibility 3 &
wait
rename_new catalog 1 &
rename_new catalog 2 &
rename_new catalog 3 &
rename_new eligibility 1 &
rename_new eligibility 2 &
rename_new eligibility 3 &
wait
