#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 DURATION_LOG OP_LOG"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home

RETRY_MAX=5
RETRY_BACKOFF=5

DURATION_LOG="$1"
OP_LOG="$2"

function finish {
	success=$?
	set +e
	echo "$(date +"%s%3N") Finish Rollback with $success"
	echo -n $(date +"%s%3N"), >> $OP_LOG
	echo -n $success >> $OP_LOG
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
	exit $success
}
trap finish EXIT

function exec_log {
	local exec_cmd="$1"
	echo "" >> $OP_LOG
	echo -n $exec_cmd, >> $OP_LOG
	echo -n $(date +"%s%3N"), >> $OP_LOG
	local exec_result=$(eval "$exec_cmd 2>&1" || true)
	echo -n $(date +"%s%3N"), >> $OP_LOG
	if [[ $exec_result == *"FAILED"* ]]; then
		exec_success=1
	else
		exec_success=0
	fi
	echo -n $exec_success >> $OP_LOG
	echo $exec_result
}

function cf_exec {
	local cf_cmd="$1"
	local cmd_result=$(exec_log "$cf_cmd")
	while [[ $cmd_result == *"Not logged in"* ]] || [[ $cmd_result == *"No "*" targeted"* ]]; do
		$LOGIN_DIR/$site.sh
		cmd_result=$(exec_log "$cf_cmd")
	done
	local tried=0
	while [[ $cmd_result == *"FAILED"* ]]; do
		sleep $RETRY_BACKOFF
		cmd_result=$(exec_log "$cf_cmd")
		tried=$(($tried + 1))
		if [[ $tried = $RETRY_MAX ]]; then
			echo "Failed in $tried times retry."
			exit 1
		fi
	done
}

function update_site {
	local siteNum=$1
	local site="site"$siteNum
	export CF_HOME=$CF_HOME_DIR/$site/
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	cf_exec "cf delete catalog-UPDATING -f"
	cf_exec "cf delete eligibility-UPDATING -f"
}

echo "" >> $DURATION_LOG
echo -n rollback, >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG
update_site 1
update_site 2
update_site 3
