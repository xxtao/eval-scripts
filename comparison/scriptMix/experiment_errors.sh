#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 EXP_TIMES ERROR_TYPE(back or forward)"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

SCRIPT_DIR=$BASE_DIR/comparison/scriptMix
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log
ERROR_MSG=$BASE_DIR/tmp/script_error.txt

EXP_TIMES=$1
ERROR_TYPE="$2"

UPD_USER="updateuser"

SUFFIX="script_"$ERROR_TYPE

LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv
OP_LOG=$DATA_DIR/duration_operations_CLI.csv

function init {
	sudo chmod -R 777 $BASE_DIR/env/login/cf_home/
	$BASE_DIR/measure.sh $SUFFIX
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
}

function finish {
	success=$?
	set +e
	$BASE_DIR/stopMeasure.sh
	exit $success
}
trap finish EXIT

init
case "${ERROR_TYPE}" in
	back)		CORRECTION_OP="$SCRIPT_DIR/rollback.sh $DURATION_LOG $OP_LOG"	;;
	forward)	CORRECTION_OP="$SCRIPT_DIR/update_mix.sh v2 $DURATION_LOG $OP_LOG"	;;
	*)
				echo "unsupported ERROR_TYPE $ERROR_TYPE"
				exit 1
esac
for i in $(eval echo {1..$EXP_TIMES}); do
	$SCRIPT_DIR/update_mix.sh v2error $DURATION_LOG $OP_LOG || true
	if [[ $(cat $ERROR_MSG) == "Start unsuccessful" ]]; then
		$CORRECTION_OP
	else
		echo "start error not triggered."
		exit 1
	fi
	if [[ $ERROR_TYPE == "forward" ]]; then
		$SCRIPT_DIR/prepare_experiment.sh
	fi
	sleep 5
done
