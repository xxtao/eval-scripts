
#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 EXP_TIMES FAILURE_TYPE(none, retry, netLong, back, forward, or kill)"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

SCRIPT_DIR=$BASE_DIR/comparison/scriptMix
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log

EXP_TIMES=$1
FAILURE_TYPE="$2"

UPD_USER="updateuser"

SUFFIX="script"
if [ $FAILURE_TYPE != none ] ; then
	SUFFIX=$SUFFIX"_"$FAILURE_TYPE
fi
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv
OP_LOG=$DATA_DIR/duration_operations_CLI.csv

function init {
	sudo chmod -R 777 $BASE_DIR/env/login/cf_home/
	$BASE_DIR/measure.sh $SUFFIX
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
	sudo chmod go+w $DURATION_LOG
}

function simulate_failure {
	case "${FAILURE_TYPE}" in
		none)
			echo "experiment without failures!"
			;;
		retry)
			echo "network kill enabled!"
			$BASE_DIR/networkKiller.sh $LOG_SUFFIX > $LOG_DIR/netkiller_log_$LOG_SUFFIX.txt 2>&1 &
			echo $! >> $PID_DIR/netkiller.pid
			;;
		netLong)
			echo "longtime network kill enabled!"
			$BASE_DIR/networkKillerLong.sh $LOG_SUFFIX > $LOG_DIR/netkillerlong_log_$LOG_SUFFIX.txt 2>&1 &
			echo $! >> $PID_DIR/netkillerlong.pid
			;;
		kill)
			echo "update process kill enabled!"
			$SCRIPT_DIR/serverKiller.sh $LOG_SUFFIX > $LOG_DIR/killer_log_$LOG_SUFFIX.txt 2>&1 &
			echo $! >> $PID_DIR/killer.pid
			;;
		*)
			echo "unsupported FAILURE_TYPE $FAILURE_TYPE"
			exit 1
	esac
}

function stop_failure {
	set +e
	case "${FAILURE_TYPE}" in
		retry)
			sudo iptables -D OUTPUT -m owner --uid-owner $UPD_USER -j REJECT #reconnect
			$BASE_DIR/kill.sh $PID_DIR/netkiller.pid ;;
		netLong)
			sudo iptables -D OUTPUT -m owner --uid-owner $UPD_USER -j REJECT #reconnect
			$BASE_DIR/kill.sh $PID_DIR/netkillerlong.pid ;;
		kill)	$BASE_DIR/kill.sh $PID_DIR/killer.pid ;;
	esac
	set -e
}

function finish {
	success=$?
	set +e
	stop_failure
	$BASE_DIR/stopMeasure.sh
	exit $success
}
trap finish EXIT

init
for i in $(eval echo {1..$EXP_TIMES}); do
	simulate_failure
	sudo -E -u $UPD_USER $SCRIPT_DIR/update_mix.sh v2 $DURATION_LOG $OP_LOG
	stop_failure
	sleep 5
	simulate_failure
	sudo -E -u $UPD_USER $SCRIPT_DIR/update_mix.sh v1 $DURATION_LOG $OP_LOG
	stop_failure
	sleep 5
done 
