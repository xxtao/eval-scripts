#!/bin/bash
set -ex

[ $# -ne 0 ] && { echo "Usage: $0"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home

function clean_site {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	apps=`cf a | sed -e '1,/name/d' | awk '{print $1;}'`
	for app in $apps; do
		cf d $app -f
	done;
	cf delete-orphaned-routes -f
}

clean_site site1
clean_site site2
clean_site site3
