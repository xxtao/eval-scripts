#!/bin/bash
set -ex

[ $# -ne 0 ] && { echo "Usage: $0"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

SCRIPT_DIR=$BASE_DIR/comparison/scriptMix
TMP_DIR=$BASE_DIR/tmp

sudo chmod -R 777 $BASE_DIR/env/login/cf_home/
$SCRIPT_DIR/clean.sh
$SCRIPT_DIR/update_straight.sh v1 $TMP_DIR/duration_prepare_script_CLI.csv

