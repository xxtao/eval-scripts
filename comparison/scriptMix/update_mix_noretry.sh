#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 VERSION DURATION_LOG OP_LOG"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
USECASE_DIR=$BASE_DIR/env/usecase

INSTANCES_NUM=3
RETRY_MAX=5
RETRY_BACKOFF=5

VERSION="$1"
DURATION_LOG="$2"
OP_LOG="$3"

function finish {
	success=$?
	set +e
	echo "$(date +"%s%3N") Finish Updating to $FINAL_STATE with $success"
	echo -n $(date +"%s%3N"), >> $OP_LOG
	echo -n $success >> $OP_LOG
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
	exit $success
}
trap finish EXIT

function cf_exec {
	local cf_cmd="$1"
	local cmd_result=$(eval "$cf_cmd 2>&1")
	if [[ $cmd_result == *"Not logged in"* ]]; then
		$LOGIN_DIR/$site.sh
		cmd_result=$(eval "$cf_cmd 2>&1")
	fi
	if [[ $cmd_result == *"No "*" targeted"* ]]; then
		$LOGIN_DIR/$site.sh
		cmd_result=$(eval "$cf_cmd 2>&1")
	fi
}

function deploy_canary {
	local app=$1
	local manifest=$app"_"$site"_"$VERSION".yml"
	local app_name=$app"-UPDATING"
	local tmp_hostname=$app"-tmp"
	cf_exec "cf push $app_name -f $manifest -i 1 --hostname $tmp_hostname -t 180"
}

function update_route {
	local app=$1
	local domain=$2
	local app_name=$app"-UPDATING"
	local tmp_hostname=$app"-tmp"
	local hostname="lizard-"$app
	cf_exec "cf map-route $app_name $domain --hostname $hostname"
	cf_exec "cf unmap-route $app_name $domain --hostname $tmp_hostname"
}

# scale up new version and scale down old version
function scale {
	local old_app=$1
	local total_instances=$2
	local new_app=$old_app"-UPDATING"
	for instances in $(eval echo {2..$total_instances}); do
		cf_exec "cf scale $new_app -i $instances"
		cf_exec "cf scale $old_app -i $((total_instances+1-instances))"
	done
}

function remove_old {
	local app=$1
	cf_exec "cf delete $app -f"
}

function rename_new {
	local app=$1
	local app_name=$app"-UPDATING"
	cf_exec "cf rename $app_name $app"
}

function update_site {
	local siteNum=$1
	local site="site"$siteNum
	local domain="cw-vdr-labs"$siteNum".elpaaso.net"
	export CF_HOME=$CF_HOME_DIR/$site/
	$LOGIN_DIR/$site.sh
	deploy_canary catalog
	deploy_canary eligibility
	update_route catalog $domain
	update_route eligibility $domain
	scale catalog $INSTANCES_NUM
	scale eligibility $INSTANCES_NUM
	remove_old catalog
	remove_old eligibility
	rename_new catalog
	rename_new eligibility
}

echo "" >> $DURATION_LOG
echo -n $VERSION, >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG
cd $USECASE_DIR
update_site 1
update_site 2
update_site 3
