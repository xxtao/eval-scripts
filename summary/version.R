#!/usr/bin/env Rscript

args = commandArgs(TRUE)
if (length(args) != 2) {
  stop("Two argument (jmeter report and duration report) must be supplied.", call.=FALSE)
} 

library(ggplot2)
jmeter_report = args[1]
timestamp_report = args[2]
dir = "./"
result_dir = "./result"
setwd(dir)

result = read.csv(jmeter_report)
initTimestamp = min(result$timeStamp)
result$timeStamp = result$timeStamp + result$Latency
# result$Latency = result$Latency / 60000
result$timeStamp = (result$timeStamp - initTimestamp) / 60000
result$success = (result$responseCode == 200)
result$response = ifelse(!result$success, "error", ifelse(is.na(result$failureMessage) | (result$failureMessage == ''), "v2", "v1"))
rps = setNames(aggregate(Latency ~ ceiling(timeStamp) + response, result, length), c("time", "response", "hits"))
summary(rps)

timestamp = read.csv(timestamp_report)[, c('starttime', 'endtime')]
timestamp$starttime = (timestamp$starttime - initTimestamp) / 60000
timestamp$endtime = (timestamp$endtime - initTimestamp) / 60000

reponseColors = c("v1"="blue", "v2"="green", "error"="red")
diagram_filename = paste(result_dir, gsub(".csv", "_version.png", basename(jmeter_report)), sep = "/")
png(diagram_filename)
ggplot() + geom_line(data = rps, aes(x = time, y = hits, color = response)) + labs(title = "request per min", x = "timestamp(min)", y = "request per min") + scale_colour_manual(name="response", values=reponseColors) + geom_vline(xintercept = timestamp$starttime, color = "black") + geom_vline(xintercept = timestamp$end, color = "black")
dev.off()