#!/usr/bin/env Rscript

library(ggplot2)
library(plyr)
library(reshape2)
library(grid) 

args = commandArgs(TRUE)
if (length(args) != 3) {
  stop("Three arguments (duration, resource and errorRate summary) must be supplied.", call.=FALSE)
} 

dir = "./"
result_dir = "./result"
setwd(dir)

duration_report <- args[1]
resource_report <- args[2]
errorRate_report <- args[3]
duration_data <- read.csv(duration_report)
resource_data <- read.csv(resource_report)
errorRate_data <- read.csv(errorRate_report)

duration_data <- subset(duration_data, method != "BG_interrupted")
resource_data <- subset(resource_data, method != "BG_interrupted")
errorRate_data <- subset(errorRate_data, method != "BG_i")
duration_data$method <- factor(duration_data$method, levels = c("Canary", "BG", "BGC", "In-place", "In-place Serial", "script"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))
resource_data$method <- factor(resource_data$method, levels = c("Canary", "BG", "BGC", "In-place", "In-place Serial", "script"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))
errorRate_data$method <- factor(errorRate_data$method, levels = c("C", "BG", "BGC", "IP", "IS","s"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))

duration_result <- ddply(duration_data, c("method"), summarise, duration = mean(duration))
resource_result <- ddply(resource_data, c("method"), summarise, resource = mean(additional_consumption)) 
errorRate_result <- ddply(errorRate_data, c("method"), summarise, errorRate = mean(errorRate) * 100)
result_data <- Reduce(function(x, y) merge(x, y, all=TRUE), list(duration_result, resource_result, errorRate_result))
print(result_data)
result_data[2,3] = 0
print(result_data)
result_data <- subset(result_data, method != "Straight(S)" & method != "script")

result_data <- melt(result_data, id = "method")
result_data$variable <- factor(result_data$variable, levels = c("duration", "errorRate", "resource"))
result_data$valueText <- result_data$value
dummy <- data.frame(method=NA, variable=rep(c("duration", "errorRate", "resource"), each=1),value=c(1.1*max(result_data$value[result_data$variable=="duration"]),1.1*max(result_data$value[result_data$variable=="errorRate"]),1.1*max(result_data$value[result_data$variable=="resource"])))

diagram_filename <- paste(result_dir, "result_v1101.png", sep = "/")
ggplot(result_data, aes(x=method, y=value, fill=variable)) + geom_bar(stat="identity", width = .4) + facet_wrap(~ variable, ncol = 1, scales = "free_y", labeller = as_labeller(c(duration="duration (min)", errorRate="errorRate (%)", resource="additional resource consumption (GB-min)"))) + labs(x="strategy",y="") + geom_text(aes(label=round(valueText, 2)),vjust=-0.5, size=8) + theme(axis.title=element_text(size=24), axis.text=element_text(size=30), strip.text = element_text(size=36),legend.title=element_blank(), legend.position="none") + geom_blank(data=dummy, aes(method, value)) #strip.position = "left",
# ggplot(result_data,aes(x=method, y=value, fill=variable)) + geom_bar(stat="identity", position=position_dodge(width = .8), colour="black") + labs(y="time (min)") + scale_y_continuous(sec.axis = sec_axis(~.*10, name = "additional resource consumption (GBmin)")) + geom_text(aes(label=round(valueText, 2)), position=position_dodge(width = .8), vjust=-0.5, size=13) + theme(axis.title=element_text(size=52), axis.title.x=element_blank(), axis.text.x=element_text(size=52), axis.text.y=element_text(size=48), legend.title=element_blank(), legend.text=element_text(size=45), legend.key.width=unit(3,"line")) + scale_fill_manual(values=c("black","grey","white"), labels=c(" duration        ", " errorRate      ", " resource")) + theme(legend.position="top") #face = "bold" #axis.text.x=element_text(size=24) #x="strategy" #+ scale_fill_grey()

ggsave(diagram_filename, width = 20, height = 15, dpi = 300) 
dev.off()
