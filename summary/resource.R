#!/usr/bin/env Rscript

args = commandArgs(TRUE)
if (length(args) != 2 ) {
  stop("Two arguments (set of resource report and duration report) must be supplied.", call.=FALSE)
} 

dir = "./"
#result_dir = "./"
setwd(dir)

result_data <- data.frame(duration=integer(), consumption=integer(), average_consumption=integer(), additional_consumption=integer(), method=factor())
num_methods <- length(args)/2
for(i in 1:num_methods){
	resource_report <- args[i] # ex. resource_prototype_BG_170731-130028.csv
	timestamp_report <- args[i+num_methods]
	resource_data <- read.csv(resource_report)
	experiment_data <- read.csv(timestamp_report)[, c('starttime', 'endtime')]
	
	reportNameSplit <- strsplit(timestamp_report, "_")[[1]]
	experiment_data$method <- paste(reportNameSplit[2:(length(reportNameSplit)-1)], collapse="_")
	
	for (i in 1:nrow(experiment_data)) {
		resource <- subset(resource_data, time >= experiment_data[i,"starttime"] & time <= experiment_data[i,"endtime"])
		consumption_data <- 0
		for (j in 2:nrow(resource)) {
			duration <- (resource[j, "time"] - resource[j-1, "time"]) / 60000
			consumption_data <- consumption_data + resource[j, "instances"] * duration
		}
		experiment_data[i, "consumption"] <- consumption_data
	}
	experiment_data$duration <- (experiment_data$endtime - experiment_data$starttime) / 60000
	experiment_data$average_consumption <- as.integer(experiment_data$consumption / experiment_data$duration + 0.5)
	if(all(resource$instances == 18)) {
		experiment_data$additional_consumption <- 0
	} else {
		experiment_data$additional_consumption <- experiment_data$consumption - experiment_data$duration * 18
	}
	result_data <- rbind(result_data, experiment_data[,names(result_data)])
}

#result_data$method <- factor(result_data$method, levels = c("prototype_BG", "prototype_BG_interrupted", "prototype_EC", "prototype_C", "prototype_I", "prototype_IS", "script"), labels = c("BG", "BG_interrupted", "Canary", "BGC", "In-place", "In-place Serial", "script"))

#result_output_filename <- paste(result_dir, "resource.csv", sep = "/")
#write.csv(result_data, file = result_output_filename, quote = FALSE, row.names=FALSE)
print(result_data)
summary(result_data)
