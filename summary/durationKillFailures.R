#!/usr/bin/env Rscript

args = commandArgs(TRUE)
if (length(args) != 1) {
  stop("One argument (duration data report) must be supplied.", call.=FALSE)
} 

report = args[1]
dir = "./"

setwd(dir)
data = read.csv(report)
data$success = (data$success==0)
data$duration = (data$endtime - data$starttime) / 1000

result <- data.frame(duration=integer(), fail_moment=integer(), success=factor())
retrying <- FALSE
retry_duration <- 0
for (i in 1:nrow(data)) {	
	if(data[i, "success"]) {
		row <- data[i, c("duration", "fail_moment", "success")]
		row["duration"] <- row["duration"] + retry_duration
		result <- rbind(result, row)
		retrying <- FALSE
		retry_duration <- 0	
	}
	else {
		retrying <- TRUE
		retry_duration <- retry_duration + data[i, "duration"]
		if((i==nrow(data)) | (data[i+1, "Af"] != data[i, "Af"])) {
			row <- data[i, c("duration", "fail_moment", "success")]
			row["duration"] <- row["duration"] + retry_duration
			row["success"] <- FALSE
			result <- rbind(result, row)
			retrying <- FALSE
			retry_duration <- 0	
		}
	}	
}

result$durationMin <- result$duration / 60
result$success <- as.factor(result$success)
print(result)
summary(result)

library(ggplot2)
dir = "./"
result_dir = "./result"
setwd(dir)
diagram_filename <- paste(result_dir, "killfail_duration.png", sep = "/")
png(diagram_filename)
ggplot(result, aes(x=fail_moment, y=durationMin)) + geom_point(aes(color=success)) + labs(title = "update execution time with killing process fault", x = "fault-free duration", y = "exec time(min)") #geom_boxplot() 
dev.off()