#!/usr/bin/env Rscript

library(ggplot2)
library(plyr)
library(reshape2)
library(grid) 

args = commandArgs(TRUE)
if (length(args) != 3) {
  stop("Three arguments (duration, resource and downtime summary) must be supplied.", call.=FALSE)
} 

dir = "./"
result_dir = "./result"
setwd(dir)

duration_report <- args[1]
resource_report <- args[2]
downtime_report <- args[3]
duration_data <- read.csv(duration_report)
resource_data <- read.csv(resource_report)
downtime_data <- read.csv(downtime_report)

duration_data <- subset(duration_data, method != "BG_interrupted")
resource_data <- subset(resource_data, method != "BG_interrupted")
downtime_data <- subset(downtime_data, method != "BG_i")
duration_data$method <- factor(duration_data$method, levels = c("Canary", "BG", "BGC", "In-place", "In-place Serial", "script"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))
resource_data$method <- factor(resource_data$method, levels = c("Canary", "BG", "BGC", "In-place", "In-place Serial", "script"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))
downtime_data$method <- factor(downtime_data$method, levels = c("C", "BG", "BGC", "IP", "IS","s"), labels = c("Canary", "BlueGreen", "Mixed", "Straight", "Straight(S)", "script"))

duration_result <- ddply(duration_data, c("method"), summarise, duration = mean(duration))
resource_result <- ddply(resource_data, c("method"), summarise, resource = mean(additional_consumption)) 
downtime_result <- ddply(downtime_data, c("method"), summarise, downtime = mean(downtimeMin))
result_data <- Reduce(function(x, y) merge(x, y, all=TRUE), list(duration_result, resource_result, downtime_result))
print(result_data)
result_data[2,3] = 0
print(result_data)
result_data <- subset(result_data, method != "Straight(S)" & method != "script")

result_data$resource <- result_data$resource / 10 # scale
result_data <- melt(result_data, id = "method")
result_data$variable <- factor(result_data$variable, levels = c("duration", "downtime", "resource"))
result_data$valueText <- result_data$value
result_data <- transform(result_data, valueText = ifelse(variable == "resource", value * 10, value))

diagram_filename <- paste(result_dir, "result.png", sep = "/")
ggplot(result_data,aes(x=method, y=value, fill=variable)) + geom_bar(stat="identity", position=position_dodge(width = .8), colour="black") + labs(y="time (min)") + scale_y_continuous(sec.axis = sec_axis(~.*10, name = "memory consumption (GB)")) + geom_text(aes(label=round(valueText, 2)), position=position_dodge(width = .8), vjust=-0.5, size=13) + theme(axis.title=element_text(size=52), axis.title.x=element_blank(), axis.text.x=element_text(size=52), axis.text.y=element_text(size=48), legend.title=element_blank(), legend.text=element_text(size=45), legend.key.width=unit(3,"line")) + scale_fill_manual(values=c("black","grey","white"), labels=c(" duration        ", " downtime      ", " resource")) + theme(legend.position="top") #face = "bold" #axis.text.x=element_text(size=24) #x="strategy" #+ scale_fill_grey()

ggsave(diagram_filename,width = 20, height = 15) 
dev.off()
