#!/bin/bash
set -ex

[ $# -ne 3 ] && { echo "Usage: $0 SCRIPT JMETER_REPORT_SUFFIX DURATION_REPORT"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

DATA_DIR="$BASE_DIR/summary/data/"

SCRIPT="./"$1
JMETER_REPORT_SUFFIX=$2
DURATION_REPORT=$3

function parse {
	jmeter_report=$DATA_DIR$1$JMETER_REPORT_SUFFIX
	$SCRIPT $jmeter_report $DURATION_REPORT
}

parse catalog_s1
parse catalog_s2
parse catalog_s3
parse eligibility_s1
parse eligibility_s2
parse eligibility_s3
