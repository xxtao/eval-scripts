#!/bin/bash
set -e

cd "$HOME/thesis/workspace/experiment/data/"
for filename in *script__*; do
	newfilename="${filename/__/_}"
	mv $filename $newfilename
done