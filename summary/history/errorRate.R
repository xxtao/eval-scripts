#!/usr/bin/env Rscript

args = commandArgs(TRUE)
if (length(args) < 1) {
  stop("More than one argument (jmeter report) must be supplied, and duration report should be in the same repo with same suffix", call.=FALSE)
} 

# library(ggplot2)
# dir = "./"
# result_dir = "./result"
# setwd(dir)

result_data <- data.frame(errorRate=integer(), microservice=factor(), site=factor(), method=factor())
for(i in 1:length(args)){
	jmeter_report <- args[i] # ex. jmeter_catalog_s1_prototype_BG_170802-185615.csv
	timestamp_report <- gsub("jmeter_[^_]+_s\\d_", "duration_", jmeter_report) 
	jmeter_data <- read.csv(jmeter_report)[, c('timeStamp', 'elapsed', 'responseCode')]
	# use request end time instead of start time to be timeStamp
	jmeter_data$timeStamp <- jmeter_data$timeStamp + jmeter_data$elapsed
	jmeter_data$success <- (jmeter_data$responseCode == 200)
	experiment_data <- read.csv(timestamp_report)[, c('starttime', 'endtime')]
	reportNameSplit <- strsplit(jmeter_report, "_")[[1]]
	experiment_data$microservice <- reportNameSplit[2]
	experiment_data$site <- reportNameSplit[3]
	experiment_data$method <- paste(reportNameSplit[4:(length(reportNameSplit)-1)], collapse="_")
		
	for(i in 1:nrow(experiment_data)) {
		durationReport <- subset(jmeter_data, timeStamp >= experiment_data[i,"starttime"] & timeStamp <= experiment_data[i,"endtime"], select = c(timeStamp, success))
		requestNum <- nrow(durationReport)
		errorNum <- nrow(durationReport[!durationReport$success,])
		experiment_data[i, "errorRate"] <- errorNum/requestNum
	}
	result_data <- rbind(result_data, experiment_data[,names(result_data)])
}

result_data$method <- factor(result_data$method, levels = c("prototype_BG", "prototype_BG_interrupted", "prototype_EC", "prototype_C", "prototype_I", "prototype_IS", "script"), labels = c("BG", "BG_i", "C", "BGC", "IP", "IS","s"))

# result_output_filename <- paste(result_dir, "errorRate.csv", sep = "/")
# write.csv(result_data, file = result_output_filename, quote = FALSE, row.names=FALSE)
print(result_data)
summary(result_data)

# diagram_filename <- paste(result_dir, "errorRate.png", sep = "/")
# png(diagram_filename)
# ggplot(result_data, aes(x = method, y = errorRate)) + geom_boxplot() + labs(title = "errorRate during update", x = "method", y = "errorRate") + facet_grid(microservice ~ site)
# dev.off()