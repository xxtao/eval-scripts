#!/bin/bash
set -e

cd "$HOME/thesis/workspace/experiment/data/"
for filename1 in *script*170804-175423*; do
    # newname=${filename/%_downtime.csv/.csv}
    # newname=downtime_$newname
    # mv $filename $newname
	echo "------------"
	filename2="${filename1/170804-175423/170807-103003}"
	newfilename1="raw_"$filename1
	newfilename2="raw_"$filename2
	mv $filename1 $newfilename1
	mv $filename2 $newfilename2
	mergedfile=$filename1
	cat $newfilename1 > $mergedfile
	echo > $mergedfile
	tail -n +2 $newfilename2 >> $mergedfile
done