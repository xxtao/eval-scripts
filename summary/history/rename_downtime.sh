#!/bin/bash

set -ex

cd "$HOME/thesis/workspace/experiment/summary/result/"
for filename in *downtime.csv; do
    newname=${filename/%_downtime.csv/.csv}
    newname=downtime_$newname
    mv $filename $newname
done