#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 SUFFIX USECASE"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

EVAL_DIR=$BASE_DIR/eval
LOG_DIR=$BASE_DIR/log

SUFFIX="$1"
USECASE="$2"

$EVAL_DIR/resource.sh $SUFFIX > $LOG_DIR/resource_log_$SUFFIX.txt 2>&1
$EVAL_DIR/loadTest_$USECASE.sh $SUFFIX > $LOG_DIR/jmeter_log_$SUFFIX.txt 2>&1