# Scripts for Evaluating the Prototype of DMU Framework
The DMU (Declarative Microservices Update) framework automates the update of multi-microservices on multi PaaS sites.
This repository evalutes the [DMU framework prototype](https://github.com/tao-xinxiu/prototype-template-engine) with three usecases: Lizard, Account and HelloWorld.

## Experiment Environment
1. Deploy three Cloud Foundry sites (cloudfoundry and diego).
2. Create the dedicated user and space for the Cloud Foundry sites with the following commands: 
```
./env/user_mgt.sh
./env/user_mgt_account.sh
``` 
3. The experiment should be run on a machine which has a network connection towards the PaaS sites. The machine should install the following applications: `java8`, `Jmeter`, `curl`.
4. Get and compile the prototype of DMU framework with the following commands:
```
git clone https://github.com/tao-xinxiu/prototype-template-engine.git
cd prototype-template-engine/
mvn clean install
```
5. Get the eval scripts repo and create the missing folder:
```
git clone https://gitlab.com/xxtao/eval-scripts.git
cd eval-scripts/
./env/prepare_folder.sh
```
6. Three environment variables should be configured on the experiment machine:
- `BASE_DIR`: the file location of this repository on the experiment machine, e.g., `$HOME/eval-scripts`
- `PROTOTYPE_DIR`: the file location of the DMU framework repository on the experiment machine, e.g., `$HOME/prototype-template-engine`
- `WITHOUT_PROXY`: whether the proxy needs to be configured for network connection. Set to `true` if no proxy is needed.  
You can configure the environment variables by adapting the file `env/eval/env_eval_cw.sh`, then executing the command `source env/eval/env_eval_cw.sh`.

## Experiments Commands
To run all the experiments, execute the following script: `nohup ./test.sh > log 2>&1 &`

Following is the command for running each specific experiment:
- evaluate the update of the usecase lizard with four strategies (Straight / BlueGreen / Canary / Mixed) for 30 times:
```
./experiment_strategies.sh S 15 lizard
./experiment_strategies.sh BG 15 lizard
./experiment_strategies.sh C 15 lizard
./experiment_strategies.sh M 15 lizard
```

- evaluate the update of the usecase account with Canary or Mixed strategy for 30 times:
```
./experiment_strategies.sh C 15 account
./experiment_strategies.sh M 15 account
```

- evaluate the update of the usecase lizard under faults (network faults / update process faults) for 30 times with 20 random value of failure-free duration: 
```
./experiment_failures.sh 15 net false 20
./experiment_failures.sh 15 kill false 20
./experiment_failures.sh 15 net true 20
./experiment_failures.sh 15 kill true 20
```

- evaluate the update of the usecase erroneous version of lizard (rollback / rollforward after detected faults) for 30 times:
```
./experiment_errors.sh 15 back
./experiment_errors.sh 15 forward
```


## Experiment output raw data
Three metrics are monitored during the update: update duration, resource consumption, and microservices availability.
Moreover another metric is also logged : for the failure cases focusing on network failure and process killing, it logs  the time when the failure is triggeered.  Then the associated  file name (containing this value) is (netkiller|killer)_prototype_[STRATEGY_INITIAL]_[FAIL_TYPE]_[TIMESTAMP].csv.



The conducted experiments result are stored in another two repositories.  
The strategies experiments which updates the usecase Lizard: [experiments data (Lizard strategies)](https://gitlab.com/xxtao/experiment/tree/master/data)  
The failures experiments of Lizard and the strategies experiments of the usecase Account: [experiments data (Lizard failures & Account strategies)](https://gitlab.com/xxtao/eval-data/)  

The experiments output the following data files to correspond to each metric. 
- update duration: `duration_(prototype|CLI_serial|script)_[STRATEGY_INITIAL](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`
- resource consumption: `resource_(prototype|CLI_serial|script)_[STRATEGY_INITIAL](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`
- microservices availability: `[MICROSERVICE_NAME]_[PAAS_SITE](_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv`

The values (meaning specified in parentheses) of the experiments output filename suffix include:
- `USECASE`: `lizard` or `account`. When nothing is specified, the "default value" is set to "Lizard"
- `MICROSERVICE_NAME`: `catalog`, `eligibility` (usecase lizard), or `account` (usecase account) 
- `STRATEGY_INITIAL`: `BG` (BlueGreen), `C`(Canary),  `M` or `BGC` (BlueGreenCanaryMixed), `I` or `S` (Straight in-place strategy performed parallel between sites), `IS` (Straight in-place strategy performed serial between sites).
- `FAIL_TYPE`: `net` (the faults of disconnecting network), `kill` (the faults of killing update process), `back` (the faults of microservices bug, fixed by roll-back), `forward` (the faults of microservices bug, fixed by roll-forward to new version) 
- `PAAS_SITE`: `s1`, `s2`, `s3`
- `prototype|CLI_serial|script`, indicates if the test was launched with DMU Framework (prototype) or directly with the Command Line Interface (CLI_serial|script). Note : *CLI_serial* and *CLI_* should not be used anymore
- `killer|netkiller`, determines the nature of the injected failure (killer=process killing, netkiller=network interruption). Only used for file storing the time of the failure triggering.


To facilitate the following summarization of the experiment result, perform the command `./summary/history/rename_jmeter.sh` to make all the output filename about microservices availability to start with "jmeter_". 

## Experiment Result Summary

The following command generates the figure (e.g., [result fig](https://gitlab.com/xxtao/experiment/blob/master/summary/result/result_v0824.png)) which summarizes all the data produced by the experiments: 

```
cd summary/
# summarize the metric "duration" of experiments data to the file "result/duration.csv"
./durationAll.R ../data/duration_prototype_*(_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv
# summarize the metric "downtime" of experiments data to the file "result/downtime.csv"
# Notice, one experiment produce multiple (Num. microservice * Num. PaaS-site) data files for microservices availability, here we need to pass all these multiple data files as parameter.
./downtimeAll.R ../data/jmeter_*(_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv
# summarize the metric "resource" of experiments data to the file "result/resource.csv"
./resourceAll.R ../data/resource_prototype_*(_[FAIL_TYPE])_[USECASE]_[TIMESTAMP].csv
# summarize all the metrics to the figure "result/result.png"
./diagramAll.R result/duration.csv result/resource.csv result/downtime.csv
```

To anaylse the experiment about network failures, generates the figure (e.g., [result fig](https://gitlab.com/xxtao/eval-data/blob/master/result/killfail_recurrent_duration.png)):
`./durationKillFailures.R ../data/duration_prototype_M_kill_[TIMESTAMP].csv`

To analyse the experiment about process failures, generates the figure (e.g., e.g., [result fig](https://gitlab.com/xxtao/eval-data/blob/master/result/netfail_recurrent_duration.png)):
`./durationNetFailures.R ../data/duration_prototype_M_net_[TIMESTAMP].csv`

## Files structure
```
|- scripts: the scripts for performing the update using the framework 
|- summary: the R scripts for analyzing the experiments result and generate diagrams 
|- helloWorld: the architecure description of a simple microservice usecase  
|- lizard: the architecure description of the microservice usecase Lizard
|- account: the architecure description of another microservice usecase Account
|- comparison 
    |- inplaceUpdate: the scripts for performing the same update using the scripts directly based on PaaS operations  
    |- push2cloud: the scripts for performing the same update using push2cloud  
|- log: the experiments log  
|- data: the raw data of the evaluation result  
|- env: scripts for preparing the execution envrionment of experiments
|- eval: scripts for monitoring the metrics during the experiment
 
```
