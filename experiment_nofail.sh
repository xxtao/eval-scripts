#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 EXP_TIMES"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log
USECASE_DIR=$BASE_DIR/env/usecase

STRATEGY_INITIAL="M"
OP_CONFIG=$STATE_DIR/operationConfig.json
UPD_USER="updateuser"

EXP_TIMES=$1

LOG_SUFFIX="prototype_"$STRATEGY_INITIAL"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function init {
	$BASE_DIR/measure.sh $LOG_SUFFIX
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
	sudo chmod go+w $DURATION_LOG
}

function finish {
	success=$?
	set +e
	$BASE_DIR/stopMeasure.sh
	exit $success
}
trap finish EXIT TERM INT

init
cd $USECASE_DIR
for i in $(eval echo {1..$EXP_TIMES}); do
	sudo -E -u $UPD_USER $SCRIPT_DIR/robustUpdate.sh $STATE_DIR/final2.json $OP_CONFIG $STRATEGY_INITIAL $DURATION_LOG $LOG_SUFFIX
	sleep 5
	sudo -E -u $UPD_USER $SCRIPT_DIR/robustUpdate.sh $STATE_DIR/final1.json $OP_CONFIG $STRATEGY_INITIAL $DURATION_LOG $LOG_SUFFIX
	sleep 5
done
