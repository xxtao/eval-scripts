#!/bin/bash
set -ex

[ $# -ne 4 ] && { echo "Usage: $0 EXP_TIMES FAILURE_TYPE(net or kill) FAIL_REGULAR(true or false) FAIL_MOMENT_NUM"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log
USECASE_DIR=$BASE_DIR/env/usecase

STRATEGY_INITIAL="M"
OP_CONFIG=$STATE_DIR/operationConfig.json
UPD_USER="updateuser"

EXP_TIMES=$1
FAILURE_TYPE="$2"
FAIL_REGULAR=$3
FAIL_MOMENT_NUM=$4

SUFFIX="prototype_"$STRATEGY_INITIAL"_"$FAILURE_TYPE
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function init {
	$BASE_DIR/measure.sh $LOG_SUFFIX
	echo -n "Af,fail_moment,fail_duration,fail_regular,starttime,endtime,success" > $DURATION_LOG
	sudo chmod go+w $DURATION_LOG
}

function simulate_failure {
	local FAIL_MOMENT=$1
	local FAIL_DURATION=$2
	case "${FAILURE_TYPE}" in
		net)
			echo "network kill enabled!"
			$BASE_DIR/killNetwork.sh $FAIL_MOMENT $FAIL_DURATION $FAIL_REGULAR $LOG_SUFFIX >> $LOG_DIR/netkiller_log_$LOG_SUFFIX.txt 2>&1 &
			echo $! >> $PID_DIR/netkiller.pid
			;;
		kill)
			echo "update process kill enabled!"
			$BASE_DIR/killUpdateProc.sh $FAIL_MOMENT $FAIL_REGULAR $LOG_SUFFIX >> $LOG_DIR/killer_log_$LOG_SUFFIX.txt 2>&1 &
			echo $! >> $PID_DIR/killer.pid
			;;
		*)
			echo "unsupported FAILURE_TYPE $FAILURE_TYPE"
			exit 1
	esac
}

function stop_failure {
	set +e
	case "${FAILURE_TYPE}" in
		net)
			sudo iptables -D OUTPUT -m owner --uid-owner $UPD_USER -j REJECT
			$BASE_DIR/kill.sh $PID_DIR/netkiller.pid ;;
		kill)
			$BASE_DIR/kill.sh $PID_DIR/killer.pid ;;
	esac
	set -e
}

function finish {
	success=$?
	set +e
	stop_failure
	$BASE_DIR/stopMeasure.sh
	exit $success
}
trap finish EXIT TERM INT

function experiment {
	local initArchi=$1
	local finalArchi=$2
	for i in {1..5}; do
		$BASE_DIR/prepare_experiment.sh $STATE_DIR/$initArchi.json lizard && break
	done;
	simulate_failure $FAIL_MOMENT $FAIL_DURATION
	for retry in {1..10}; do
		echo "" >> $DURATION_LOG
		echo -n $finalArchi,$FAIL_MOMENT,$FAIL_DURATION,$FAIL_REGULAR, >> $DURATION_LOG
		sudo -E -u $UPD_USER $SCRIPT_DIR/update.sh $STATE_DIR/$finalArchi.json $OP_CONFIG $STRATEGY_INITIAL $DURATION_LOG $LOG_SUFFIX && break
		sleep 60
	done
	stop_failure
	sleep 5
}

init
cd $USECASE_DIR
for i in $(eval echo {1..$FAIL_MOMENT_NUM}); do
	FAIL_MOMENTS[i]=$[ ( $RANDOM % 360 ) + 1 ]
done
FAIL_DURATIONS=(1 15 60) #(0.1 1 5 15 60)
for exp in $(eval echo {1..$EXP_TIMES}); do
	for FAIL_MOMENT in ${FAIL_MOMENTS[@]}; do
		for FAIL_DURATION in ${FAIL_DURATIONS[@]}; do
			experiment final1 final2
			experiment final2 final1
		done
	done
done
