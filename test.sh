#!/bin/bash
set -ex

BASE_DIR="$(dirname "$0")"
cd "$BASE_DIR"
source env/eval/env_eval_cw.sh

TIME_SUFFIX=$(date +%y%m%d-%H%M%S)

### experiments for updating the usecase lizard with four strategies (Straight / BlueGreen / Canary / Mixed)
./cleanup.sh lizard
./cleanup.sh account
./prepare_experiment.sh $BASE_DIR/lizard/final1.json lizard
./experiment_strategies.sh S 15 lizard > log_prototype_S_$TIME_SUFFIX 2>&1
./experiment_strategies.sh BG 15 lizard > log_prototype_BG_$TIME_SUFFIX 2>&1
./experiment_strategies.sh C 15 lizard > log_prototype_C_$TIME_SUFFIX 2>&1
./experiment_strategies.sh M 15 lizard > log_prototype_M_$TIME_SUFFIX 2>&1

### experiments for updating lizard under faults (network faults / update process faults)
#./experiment_nofail.sh 30 > log_prototype_none_$TIME_SUFFIX 2>&1
./experiment_failures.sh 15 net false 20 > log_prototype_net_once_$TIME_SUFFIX 2>&1
./experiment_failures.sh 15 kill false 20 > log_prototype_kill_once_$TIME_SUFFIX 2>&1
./experiment_failures.sh 15 net true 20 > log_prototype_net_regular_$TIME_SUFFIX 2>&1
./experiment_failures.sh 15 kill true 20 > log_prototype_kill_regular_$TIME_SUFFIX 2>&1

### experiments for updating erroneous version of lizard (rollback / rollforward after detected faults)
./experiment_errors.sh 15 back > log_prototype_back_$TIME_SUFFIX 2>&1
./experiment_errors.sh 15 forward > log_prototype_forward_$TIME_SUFFIX 2>&1

### experiments for updating the usecase account with Canary or Mixed strategy
./cleanup.sh lizard
./prepare_experiment.sh $BASE_DIR/account/final1.json account
./experiment_strategies.sh C 15 account > log_prototype_C_$TIME_SUFFIX 2>&1
./experiment_strategies.sh M 15 account > log_prototype_C_$TIME_SUFFIX 2>&1
