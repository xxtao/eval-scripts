#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 EXP_TIMES ERROR_TYPE(back or forward)"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log
USECASE_DIR=$BASE_DIR/env/usecase

STRATEGY_INITIAL="M"
ROLLBACK_STRATEGY="S"
OP_CONFIG=$STATE_DIR/operationConfigLessTO.json

EXP_TIMES=$1
ERROR_TYPE="$2"

SUFFIX="prototype_"$STRATEGY_INITIAL"_"$ERROR_TYPE
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function init {
	$BASE_DIR/measure.sh $SUFFIX
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
}

function finish {
	success=$?
	set +e
	$BASE_DIR/stopMeasure.sh
	exit $success
}
trap finish EXIT

init
cd $USECASE_DIR
case "${ERROR_TYPE}" in
	back)
		CORRECTION_STATE=$STATE_DIR/final1.json
		CORRECTION_STRATEGY=$ROLLBACK_STRATEGY
		;;
	forward)
		CORRECTION_STATE=$STATE_DIR/final2.json
		CORRECTION_STRATEGY=$STRATEGY_INITIAL
		;;
	*)
		echo "unsupported ERROR_TYPE $ERROR_TYPE"
		exit 1
esac
for i in $(eval echo {1..$EXP_TIMES}); do
	$SCRIPT_DIR/correctUpdate.sh $CORRECTION_STATE $CORRECTION_STRATEGY $STATE_DIR/final2error.json $STRATEGY_INITIAL $DURATION_LOG $LOG_SUFFIX
	sleep 5
	if [[ $ERROR_TYPE == "forward" ]]; then
		$BASE_DIR/prepare_experiment.sh $STATE_DIR/final1.json lizard
	fi
done
