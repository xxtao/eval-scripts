#!/bin/bash
set -ex

[ $# -ne 4 ] && { echo "Usage: $0 MOMENT DURATION REGULAR LOG_SUFFIX"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

DATA_DIR=$BASE_DIR/data
UPD_USER=updateuser

MOMENT=$1
DURATION=$2
REGULAR=$3
LOG_SUFFIX=$4
TIMESTAMP_DATA=$DATA_DIR/netkiller_$LOG_SUFFIX.csv

function disconnect {
	sudo iptables -A OUTPUT -m owner --uid-owner $UPD_USER -j REJECT
}

function connect {
	sudo iptables -D OUTPUT -m owner --uid-owner $UPD_USER -j REJECT
}

[[ -f $TIMESTAMP_DATA ]] || echo "killTime,recoveryTime" > $TIMESTAMP_DATA

while true; do
	sleep $MOMENT # wait MOMENT sec before kill network
	disconnect
	echo -n $(date +"%s%3N"), >> $TIMESTAMP_DATA
	echo "Network Killed!"
	sleep $DURATION # wait DURATION sec before recover network
	connect
	echo $(date +"%s%3N") >> $TIMESTAMP_DATA
	echo "Network Recovered!"
	[[ $REGULAR != true ]] && break
done
