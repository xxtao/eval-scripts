#!/bin/bash
set -x
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

EVAL_DIR=$BASE_DIR/eval
PID_DIR=$BASE_DIR/tmp

$BASE_DIR/kill.sh $PID_DIR/resource.pid
$EVAL_DIR/stopTest.sh