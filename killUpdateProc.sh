#!/bin/bash
set -ex

[ $# -ne 3 ] && { echo "Usage: $0 MOMENT REGULAR LOG_SUFFIX"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp

MOMENT=$1
REGULAR=$2
LOG_SUFFIX=$3
TIMESTAMP_DATA=$DATA_DIR/killer_$LOG_SUFFIX.csv

[[ -f $TIMESTAMP_DATA ]] || echo "killTime,recoveryTime" > $TIMESTAMP_DATA

while true; do
	sleep $MOMENT # wait MOMENT sec before kill framework proc
	$BASE_DIR/kill.sh $PID_DIR/framework.pid
	echo $(date +"%s%3N") >> $TIMESTAMP_DATA
	echo "Update Process Killed!"
	[[ $REGULAR != true ]] && break
done
