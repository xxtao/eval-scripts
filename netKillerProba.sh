#!/bin/bash
set -ex

[ $# -ne 0 ] && { echo "Usage: $0"; exit 1; }

UPD_USER=updateuser

function disconnect {
	sudo iptables -A OUTPUT -m owner --uid-owner $UPD_USER -m statistic --mode random --probability 0.1 -j REJECT
}

function connect {
	sudo iptables -D OUTPUT -m owner --uid-owner $UPD_USER -m statistic --mode random --probability 0.1 -j REJECT
}
