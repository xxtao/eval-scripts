#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 FILENAME_SUFFIX"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

TEMPLATE_DIR=$BASE_DIR/eval/template
ENV_DIR=$BASE_DIR/scripts/env
TMP_DIR=$BASE_DIR/eval/tmp

FILENAME_SUFFIX=$1

if [[ -v "WITHOUT_PROXY" ]]; then
	DATA_DIR=$BASE_DIR/data/
	env=$ENV_DIR/env_linux_noproxy.sh
else
	unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)     
			DATA_DIR=$BASE_DIR/data/
			env=$ENV_DIR/env_linux.sh
			;;
	    MINGW*)     
			DATA_DIR='C://Users//JGSJ1658//thesis//workspace//experiment//data//' 
			env=$ENV_DIR/env_windows.sh
			;;
	    *)          
			exit 1
	esac
fi

shopt -s expand_aliases
source $env

function measure_site {
	local csv_filename=$DATA_DIR$1"_"$FILENAME_SUFFIX".csv"
	local template_filename=$TEMPLATE_DIR/$1.jmx
	local jmeter_filename=$TMP_DIR/$1"_"$FILENAME_SUFFIX".jmx"
	echo "func: fillout output_filename $csv_filename into template $template_filename"
	sed -e "s|{OUTPUT_FILENAME}|$csv_filename|g" $template_filename > $jmeter_filename
	jmeter -n -t $jmeter_filename
}
measure_site catalog_s1 &
measure_site catalog_s2 &
measure_site catalog_s3 &
measure_site eligibility_s1 &
measure_site eligibility_s2 &
measure_site eligibility_s3 &
