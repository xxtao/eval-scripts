#!/bin/bash
set -x

[ $# -ne 1 ] && { echo "Usage: $0 RESOURCE_LOG_SUFFIX"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
LOG_DIR=$BASE_DIR/data
PID_FILE=$BASE_DIR/tmp/resource.pid

RESOURCE_LOG_SUFFIX="$1"

SITE1_SPACE_GUID="b3f5b6e4-d5b4-4bbf-a5f5-d3b283204c9e"
SITE2_SPACE_GUID="59f49c68-8ebc-48fe-8b79-9d256d168c83"
SITE3_SPACE_GUID="0905a9f8-af57-4361-949e-4349c75d27ee"

function get_site_instances {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	local space_guid=$2
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	site_instances=$(cf curl v2/spaces/$space_guid/apps | jq .resources[].entity.instances | jq -s 'add')
}

function log_sum_instances {
	RESOURCE_LOG=$LOG_DIR/"resource_"$RESOURCE_LOG_SUFFIX".csv"
	echo -n "time,instances" > $RESOURCE_LOG
	while true; do
		echo "" >> $RESOURCE_LOG
		echo -n $(date +"%s%3N"), >> $RESOURCE_LOG
		sum_instances=0
		get_site_instances site1 $SITE1_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		get_site_instances site2 $SITE2_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		get_site_instances site3 $SITE3_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		echo -n $sum_instances >> $RESOURCE_LOG
	done;
}

sudo chmod -R 777 $BASE_DIR/env/login/cf_home/
log_sum_instances &
echo $(jobs -pr) >> $PID_FILE
