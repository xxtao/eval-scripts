#!/bin/bash
set -x
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

TMP_DIR="$BASE_DIR/eval/tmp"

shutdown.sh 4445
shutdown.sh 4446
shutdown.sh 4447
shutdown.sh 4448
shutdown.sh 4449
shutdown.sh 4450

rm $TMP_DIR/catalog_s*.jmx
rm $TMP_DIR/eligibility_s*.jmx
