#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 FINAL_STATE USECASE"; exit 1; }
[[ -z "${BASE_DIR}" ]] && { echo "Missing env var: BASE_DIR"; exit 1; }

STATE_DIR=$BASE_DIR/$2
SCRIPT_DIR=$BASE_DIR/scripts
USECASE_DIR=$BASE_DIR/env/usecase

LOG_SUFFIX="tmp"
OP_CONFIG=$STATE_DIR/operationConfig.json
DURATION_LOG=$BASE_DIR/tmp_duration_prepare.csv
LOG_SUFFIX=prepare
FINAL_STATE=$1
USECASE=$2
case "$USECASE" in
	lizard)		STRATEGY=S	;;
	account)	STRATEGY=C	;;
	* )			exit 1		;;
esac

cd $USECASE_DIR
$SCRIPT_DIR/update.sh $FINAL_STATE $OP_CONFIG $STRATEGY $DURATION_LOG $LOG_SUFFIX $2
